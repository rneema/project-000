Project: Title
==============

Participants
============

Lastname, Firstname, HID, gitlab_username, e-mail

Abstract
========

TBD

Refernces
=========

URL to bibtex file that is maintained via jabref

Deliverables
============

Use subdirectories to cleanly manage your deliverables where appropriate

* Easy **short** instalation instructions
* Easy **short** script that lets us run the application possibly on a subset of 
  data
* Program to fetch the data (data should not relay be stored here, we will stor 
  this elsewhere)
* Program to actually conduct the calculation 
* Report in original format
* Report in PDF
* Images directory with all images included in the report

Recomendations
==============

We recommend that you use sharelatex or edit the report in latex directly in the 
git repository. Make sure your team mambers knwo how to do that. If not indicate 
how you will manage your team report.

Check back with project-000 for tips that we have not yet included here

Changelog
=========

* edit from vm
* change bey another user
